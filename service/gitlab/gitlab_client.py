import requests
import json

from typing import List
from issue import Issue, IssueToCreate


class GitlabClient:

    def __init__(self, private_token: str, token_name: str):
        self.private_token = private_token
        self.token_name = token_name

    id_project = 51938167

    def get_open_issues() -> List[Issue]:
        url = "https://gitlab.com/api/v4/projects/" + \
            str(GitlabClient.id_project) + "/issues?state=opened"
        response = requests.get(url)
        if response.status_code == 200:
            return response.json()
        else:
            return []

    def get_open_issues_for_filename(filename: str) -> List[Issue]:
        url = "https://gitlab.com/api/v4/projects/" + \
            str(GitlabClient.id_project) + \
            f"/issues?state=opened&search={filename}&in=title"
        response = requests.get(url)
        jsonparsed = json.loads(response.text)
        return jsonparsed
    
    def create_issue(self, issue: IssueToCreate):
        url = "https://gitlab.com/api/v4/projects/" + \
            str(GitlabClient.id_project) + "/issues"
        payload = json.loads(json.dumps(issue.__dict__, default=lambda o: o.__dict__))
        headers = {self.token_name: self.private_token}
        response = requests.post(url, headers=headers, json=payload)

        print(response.status_code)

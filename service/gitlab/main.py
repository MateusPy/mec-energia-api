import json
from os import environ as env
from typing import List
from coverage import Coverage
from pathlib import Path
from pytest import main as pytest

from issue import Issue
from gitlab_client import GitlabClient
from issue import IssueToCreate


def get_env(key: str) -> str:
    try:
        return env[key]
    except KeyError as e:
        raise KeyError(f'Missing variable: {e}')


def print_color_message(message, color_code='reset'):
    colors = {
        "ERROR": "\033[91m",
        "OK": "\033[92m",
        "WARN": "\033[93m",
        "INFO": "\033[94m",
        "reset": "\033[0m"
    }

    color = colors.get(color_code, colors["reset"])
    print(f"{color}{message}{colors['reset']}")


def create_issues(iss: List[IssueToCreate]):
    # def create_issues(iss: List[Issue]):
    private_token = get_env('ISSUE_TOKEN')
    token_name = get_env('ISSUE_TOKEN_USER')
    api = GitlabClient(private_token, token_name)
    for i in iss:
        log(f'Will create issue for file: {i.title}', 'info')
        # api.create_issue(i)


def log(msg: str, lvl='INFO'):
    print_color_message(msg, lvl.upper())


def run_tests():

    try:
        base_path = get_env('BASE_PATH')

    except KeyError:
        base_path = str(Path('.').absolute())

    min_cov = int(get_env('MIN_COV'))

    print(
        f'Starting to test for minimum coverage of {min_cov}% in path={base_path}.')

    file = Path('.coverage')
    configfile = Path('.coveragerc')

    if configfile.is_file():
        print(f'Loading configs from: {configfile.absolute()}')

    if not file.is_file():
        pytest()

    print(f'Loading configs from: {file.absolute()}')

    cov = Coverage(data_file=file, config_file=configfile)
    cov.load()
    cov.json_report()
    p = Path(cov.get_option('json:output'))

    with p.open('r', encoding='utf-8') as p:
        j = json.load(p)

    files = j['files']
    iss: List[IssueToCreate] = []
    for filename, data in files.items():
        cov = float(data['summary']['percent_covered'])

        if cov < min_cov:
            lvl = 'error'
            title = f'{filename}'
            description = f'Coverage report on file: {filename} is below minimum ({min_cov}%).'
            labels = ['auto-generated']
            iss.append(IssueToCreate(title, description, labels))
        elif cov == min_cov:
            lvl = 'warn'
        else:
            lvl = 'ok'

        msg = f'{filename} {cov}'
        log(msg, lvl)
    create_issues(iss)


def main():
    run_tests()


try:
    main()
except Exception as e:
    print(e)

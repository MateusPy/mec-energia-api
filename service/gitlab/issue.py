from dataclasses import dataclass
from typing import List, Optional

@dataclass
class Author:
    id: int
    username: str
    name: str
    state: str
    locked: bool
    avatar_url: str
    web_url: str

@dataclass
class TimeStats:
    time_estimate: int
    total_time_spent: int
    human_time_estimate: Optional[int]
    human_total_time_spent: Optional[int]

@dataclass
class TaskCompletionStatus:
    count: int
    completed_count: int

@dataclass
class Links:
    self: str
    notes: str
    award_emoji: str
    project: str
    closed_as_duplicate_of: Optional[str]

@dataclass
class References:
    short: str
    relative: str
    full: str

@dataclass
class Issue:
    id: int
    iid: int
    project_id: int
    title: str
    description: str
    state: str
    created_at: str
    updated_at: str
    closed_at: Optional[str]
    closed_by: Optional[str]
    labels: List[str]
    milestone: Optional[str]
    assignees: List[str]
    author: Author
    type: str
    assignee: Optional[str]
    user_notes_count: int
    merge_requests_count: int
    upvotes: int
    downvotes: int
    due_date: Optional[str]
    confidential: bool
    discussion_locked: Optional[str]
    issue_type: str
    web_url: str
    time_stats: TimeStats
    task_completion_status: TaskCompletionStatus
    weight: Optional[str]
    blocking_issues_count: int
    has_tasks: bool
    task_status: str
    links: Links
    references: References
    severity: str
    moved_to_id: Optional[str]
    service_desk_reply_to: Optional[str]
    epic_iid: Optional[str]
    epic: Optional[str]
    iteration: Optional[str]
    health_status: Optional[str]

@dataclass
class IssueToCreate:
    title: str
    description: str
    labels: List[str]